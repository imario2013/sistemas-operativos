/*
Ejemplo de hilos con parametros
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


void *ImprimirSecuencia(void *args){
	int *parametro = (int *)args;
	int i;

	printf("El parametro enviado es: %d\n", *parametro);
	for(i=0;i<*parametro; i++)
	{
		fflush(stdout);
		printf("5");
		sleep(1);
	}
	
}

int main (void)
{
	pthread_t hilo1;
	
	//variable que almacena el valor del parametro
	int parametro = 5;
	//puntero a la variable parametro
	int *p = &parametro;
	
	/*
	printf("El parametro enviado es: %d\n", parametro);
	printf("El parametro enviado es: %d\n", *p);
	*/

	/*
	Debido a que la función "ImprimirSecuencia" recibe argumentos de tipo (void *) al enviar el 		  
	parametro que será un puntero hacemos un cast del puntero int a (void *).
	Lo argumentos son enviados de este tipo para que la función que ejecutará el hilo reciba cualquier tipo de parametro.
	*/
	pthread_create(&hilo1, NULL, ImprimirSecuencia, (void*)p);
	pthread_join(hilo1, NULL);
	
	printf("\nFin de la ejecución\n");
	return 0;
}



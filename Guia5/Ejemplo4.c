/*
Ejemplo de hilos con parametros
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>


void *ImprimirSecuencia(void *args){
	char *parametro = (char *)args;
	int i;

	printf("El parametro enviado es: %s\n", parametro);
	printf("------------------------------------\n");
	for(i=0;i<strlen(parametro); i++)
	{
		fflush(stdout);
		printf("%c", parametro[i]);
		sleep(1);
	}
	printf("\n------------------------------------\n");
	
}

int main (void)
{
	pthread_t hilo1;
	char *parametro = "Buenas tardes estimados";
	
	pthread_create(&hilo1, NULL, ImprimirSecuencia, (void*)parametro);
	pthread_join(hilo1, NULL);
	
	printf("\nFin de la ejecución\n");
	return 0;
}



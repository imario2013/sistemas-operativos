/*
Ejemplo de hilos
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>


void *ImprimirSecuencia1(void *args){
	int i;
	for(i=0;i<10; i++)
	{
		fflush(stdout);
		printf("5");
		sleep(1);
	}
	
}

void *ImprimirSecuencia2(void *args){
	int i;
	for(i=0;i<10; i++)
	{
		fflush(stdout);
		printf("7");
		sleep(1);
	}
	
}

int main (void)
{
	pthread_t hilo1;
	pthread_t hilo2;
	
	if(pthread_create(&hilo1, NULL, ImprimirSecuencia1, NULL) || pthread_create(&hilo1, NULL, ImprimirSecuencia2, NULL))
	{
		printf("Error creando hilos");
		abort();
	}

	pthread_join(hilo1, NULL);
	pthread_join(hilo2, NULL);
	printf("\nFin del join\n");
	
	return 0;
}



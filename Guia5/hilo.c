/*
Ejemplo de hilos
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>


void *hilo(void *args){
	int i;
	for(i=0;i<10; i++)
	{
		//fflush(stdout);
		printf("Hilo #%d\n",i);
		sleep(1);
	}
	
}

int main (void)
{
	pthread_t mihilo;
	
	if(pthread_create(&mihilo, NULL, hilo, NULL))
	{
		printf("Error creando el hilo");
		abort();
	}
	
	for(int i=0;i<30; i++)
	{
		//fflush(stdout);
		printf("Proceso #%d\n",i);
		sleep(1);
	}

    printf("Esperando unión de hilos (join)\n");
    if(pthread_join(mihilo, NULL)){
        printf("\nError uniendo los hilos\n");
    }
	printf("Fin de la unión de los hilos\n");

	return 0;
}

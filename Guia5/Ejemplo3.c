/*
Ejemplo de hilos con parametros
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


typedef struct {
	char *mensaje;
	char *usuario;
	int n;
} parametro;

void *ImprimirMensaje(void *args){
	parametro *p = (parametro *)args;
	int i;
	
	printf("Valores del parametro\n");
	printf("Mensaje: %s\n", p->mensaje);
	printf("Usuario: %s\n", p->usuario);
	printf("N: %d\n\n", p->n);

	for(i=0;i<p->n; i++)
	{
		fflush(stdout);
		printf("----------------------------------------------------\n");
		printf("Iteración #%d\n", (i+1));
		printf("Mensaje: %s\n", p->mensaje);
		printf("Usuario: %s\n", p->usuario);
		printf("----------------------------------------------------\n");
		sleep(1);
	}
	
}

int main (void)
{
	pthread_t hilo1;
	
	//variable de tipo parametro
	parametro p1;
	p1.mensaje = "Hello darkness my old friend";
	p1.usuario = "Mario Meléndez";
	p1.n = 2;	

	//puntero de tipo parametro
	parametro *p = &p1;

	pthread_create(&hilo1, NULL, ImprimirMensaje, (void*)p);
	pthread_join(hilo1, NULL);
	
	printf("\nFin de la ejecución\n");
	return 0;
}



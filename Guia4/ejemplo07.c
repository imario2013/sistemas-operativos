#include <unistd.h>
#include <stdio.h>

main(){
    int pid = fork();

    if(pid>0){
        printf("El pid del proceso padre es: %d\n", getpid());
    }
}
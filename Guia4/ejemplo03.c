#include <unistd.h>
#include <stdio.h>

main(){
    /* 
    Con la funcíon fork vamos a crear una copia del 
    proceso que se esta ejecutando 
    */
    fork();
    printf("Hola alumnos de Sistemas Operativos\n");
}